package net.analizer.hackernews;

import android.os.Build;

import net.analizer.data.models.DataStoreFactory;
import net.analizer.data.models.OfflineDataStoreImp;
import net.analizer.data.models.OnlineDataStoreImp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = net.analizer.data.BuildConfig.class,
        sdk = Build.VERSION_CODES.N_MR1,
        application = NewsFeedApp.class
)
public class DataStoreFactoryTest extends BaseDataStoreTest {

    @Test
    public void createDataStore_WhereDatabaseIsEmpty_ShouldReturnOnlineDataStoreInstance() {
        DataStoreFactory factory = new DataStoreFactory(daoSession);
        assertThat(factory.createDataStore(null)).isInstanceOf(OnlineDataStoreImp.class);
    }

    @Test
    public void createDataStore_WhereDatabaseIsPopulated_ShouldReturnOfflineDataStoreInstance() {
        insertStories();
        checkIfStoriesAreInserted();

        DataStoreFactory factory = new DataStoreFactory(daoSession);
        assertThat(factory.createDataStore(null)).isInstanceOf(OfflineDataStoreImp.class);
    }

    @Test
    public void createDataStore_WhereCommentExistInDatabase_ShouldReturnOfflineDataStoreInstance() {
        insertStories();
        checkIfStoriesAreInserted();

        DataStoreFactory factory = new DataStoreFactory(daoSession);
        assertThat(factory.createDataStore(existingComment)).isInstanceOf(OfflineDataStoreImp.class);
    }

    @Test
    public void createDataStore_WhereCommentNotExistInDatabase_ShouldReturnOnlineDataStoreInstance() {
        insertStories();
        checkIfStoriesAreInserted();

        DataStoreFactory factory = new DataStoreFactory(daoSession);
        assertThat(factory.createDataStore(notExistingComment)).isInstanceOf(OnlineDataStoreImp.class);
    }

    @Test
    public void createDataStore_WhereStoryExistInDatabase_ShouldReturnOfflineDataStoreInstance() {
        insertStories();
        checkIfStoriesAreInserted();

        DataStoreFactory factory = new DataStoreFactory(daoSession);
        assertThat(factory.createDataStore(storyWithNoComment)).isInstanceOf(OfflineDataStoreImp.class);
    }

    @Test
    public void createDataStore_WhereStoryNotExistInDatabase_ShouldReturnOnlineDataStoreInstance() {
        insertStories();
        checkIfStoriesAreInserted();

        DataStoreFactory factory = new DataStoreFactory(daoSession);
        assertThat(factory.createDataStore(notExistingStory)).isInstanceOf(OnlineDataStoreImp.class);
    }
}