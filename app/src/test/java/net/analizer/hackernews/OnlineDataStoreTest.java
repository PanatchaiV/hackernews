package net.analizer.hackernews;

import android.os.Build;

import net.analizer.data.models.OnlineDataStoreImp;
import net.analizer.domain.DataStore;
import net.analizer.domain.models.FeedItem;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Arrays;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = net.analizer.data.BuildConfig.class,
        sdk = Build.VERSION_CODES.N_MR1,
        application = NewsFeedApp.class
)
public class OnlineDataStoreTest extends BaseDataStoreTest {

    protected DataStore onlineData;

    private FeedItem onlineStory;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        onlineData = new OnlineDataStoreImp();
        onlineStory = new FeedItem(16435949L, null, FeedItem.TYPE_STORY, "rbanffy", 1519285650L, null, null, null, null, Arrays.asList(16436856L, 16436458L, 16436775L, 16436743L, 16436752L, 16436805L), null, 83, "Computer History Museum 2018 Fellow Award – Guido van Rossum", null, 9L);
    }

    @Test
    public void getTopStories_ReturnTopStories() {
        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = onlineData.getTopStories();
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
    }

    @Test
    public void getAnExistingFeedItem_ReturnTheRequestedFeed() {
        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = onlineData.getItemDetail(onlineStory.id);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertValue(onlineStory);
    }

    @Test
    public void getNonExistingFeedItem_ThrowError() {
        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = onlineData.getItemDetail(notExistingStory.id);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertError(Throwable.class);
    }

    @Test
    public void getCommentOfAnExistingStory_ReturnCommentList() {

        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = onlineData.getComment(onlineStory);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        assertThat(testObserver.valueCount()).isGreaterThan(1);
    }

    @Test
    public void getCommentFromStoryWithoutComment_ThrowError() {
        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = onlineData.getComment(storyWithNoComment);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertError(Throwable.class);
    }

    @Test
    public void getCommentFromNonExistingStory_ThrowError() {
        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = onlineData.getComment(notExistingStory);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertError(Throwable.class);
    }
}
