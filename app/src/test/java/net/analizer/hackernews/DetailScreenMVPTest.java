package net.analizer.hackernews;

import android.os.Build;

import net.analizer.domain.DataRepo;
import net.analizer.domain.models.FeedItem;
import net.analizer.hackernews.mvp.detail.DetailPresenterImpl;
import net.analizer.hackernews.mvp.detail.DetailScreen;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import io.reactivex.Observable;

import static org.mockito.ArgumentMatchers.any;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = net.analizer.data.BuildConfig.class,
        sdk = Build.VERSION_CODES.N_MR1,
        application = NewsFeedApp.class
)
public class DetailScreenMVPTest extends BaseMVPTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    DetailScreen.DetailView view;

    @Mock
    DataRepo dataRepo;

    @Mock
    FeedItem mockFeedData;

    @Mock
    FeedItem mockComment;

    DetailScreen.DetailPresenter detailPresenter;

    @Before
    public void setUp() throws Exception {
        detailPresenter = new DetailPresenterImpl(dataRepo);
        detailPresenter.setView(view);
    }

    @Test
    public void onFeedReady_ShouldDisplayFeedDetail_And_StartLoadingAndDisplayComment() {

        Mockito
                .when(dataRepo.getComment(mockFeedData))
                .thenReturn(Observable.just(mockComment));

        detailPresenter.startLoadingFeedComment(mockFeedData);
        Mockito.verify(view).displayFeed(mockFeedData);
        Mockito.verify(view).displayComment(mockComment);
        Mockito.verify(view, Mockito.never()).displayError(any());
    }

    @Test
    public void onFeedReady_ButThereIsAnError_ShouldDisplayError() {

        Throwable throwable = new Throwable("Some Random Error");
        Mockito
                .when(dataRepo.getComment(mockFeedData))
                .thenReturn(Observable.error(throwable));

        detailPresenter.startLoadingFeedComment(mockFeedData);
        Mockito.verify(view).displayFeed(mockFeedData);
        Mockito.verify(view).displayError(throwable.getMessage());
        Mockito.verify(view, Mockito.never()).displayComment(any());
    }
}