package net.analizer.hackernews;

import android.os.Build;

import net.analizer.domain.DataRepo;
import net.analizer.domain.models.FeedItem;
import net.analizer.hackernews.mvp.list.ListScreen;
import net.analizer.hackernews.mvp.list.ListScreenPresenterImpl;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import io.reactivex.Observable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = net.analizer.data.BuildConfig.class,
        sdk = Build.VERSION_CODES.N_MR1,
        application = NewsFeedApp.class
)
public class ListScreenMVPTest extends BaseMVPTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    ListScreen.ListScreenView view;

    @Mock
    DataRepo dataRepo;

    @Mock
    FeedItem mockFeedData1;

    @Mock
    FeedItem mockFeedData2;

    ListScreen.ListScreenPresenter listScreenPresenter;

    @Before
    public void setUp() throws Exception {
        listScreenPresenter = new ListScreenPresenterImpl(dataRepo);
        listScreenPresenter.setView(view);
    }

    @Test
    public void onFreshStartActivity_ShouldStartLoadingFeed_And_DisplayFeedItemOneAfterAnother() {

        when(dataRepo.getTopStories()).thenReturn(Observable.fromArray(mockFeedData1, mockFeedData2));
        listScreenPresenter.startLoadingFeed();
        Mockito.verify(view, times(2)).displayFeed(any());
        Mockito.verify(view).displayFeed(mockFeedData1);
        Mockito.verify(view).displayFeed(mockFeedData2);
    }

    @Test
    public void onFeedClicked_ShouldDisplayDetail() {
        when(dataRepo.getTopStories()).thenReturn(Observable.just(mockFeedData1));
        listScreenPresenter.startLoadingFeed();
        Mockito.verify(view).displayFeed(mockFeedData1);
        listScreenPresenter.onFeedClick(mockFeedData1);
        Mockito.verify(view).displayFeedDetail(mockFeedData1);
    }
}