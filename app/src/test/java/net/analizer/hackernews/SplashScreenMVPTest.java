package net.analizer.hackernews;

import android.os.Build;

import net.analizer.domain.DataRepo;
import net.analizer.domain.models.FeedItem;
import net.analizer.hackernews.mvp.splash.SplashScreen;
import net.analizer.hackernews.mvp.splash.SplashScreenPresenterImpl;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import io.reactivex.Observable;

import static org.mockito.ArgumentMatchers.any;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = net.analizer.data.BuildConfig.class,
        sdk = Build.VERSION_CODES.N_MR1,
        application = NewsFeedApp.class
)
public class SplashScreenMVPTest extends BaseMVPTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    SplashScreen.SplashScreenView view;

    @Mock
    DataRepo dataRepo;

    @Mock
    FeedItem mockFeedData;

    SplashScreen.SplashScreenPresenter splashScreenPresenter;

    @Before
    public void setUp() throws Exception {
        splashScreenPresenter = new SplashScreenPresenterImpl(dataRepo);
        splashScreenPresenter.setView(view);
    }

    @Test
    public void startLoading_ShouldDisplayProgress() {
        Mockito.when(dataRepo.getTopStories()).thenReturn(Observable.just(mockFeedData));

        splashScreenPresenter.onStartLoading();
        Mockito.verify(view).showLoading();
        Mockito.verify(dataRepo).getTopStories();
        Mockito.verify(view).showMessage(any());
        Mockito.verify(view).dismissLoading();
        Mockito.verify(view).showNextView(any());
    }

    @Test
    public void onLoadError_ShouldDisplayErrorMsg() {

        Mockito.when(dataRepo.getTopStories()).thenReturn(Observable.just(mockFeedData), Observable.error(new Throwable("Random Error")));
        splashScreenPresenter.onStartLoading();
        Mockito.verify(view).showLoading();
        Mockito.verify(dataRepo).getTopStories();
        Mockito.verify(view).dismissLoading();
        Mockito.verify(view).showMessage(any());
    }
}