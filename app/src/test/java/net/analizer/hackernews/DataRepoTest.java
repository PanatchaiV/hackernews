package net.analizer.hackernews;

import android.os.Build;

import net.analizer.data.models.DataRepoImp;
import net.analizer.domain.DataRepo;
import net.analizer.domain.models.FeedItem;

import org.greenrobot.greendao.query.QueryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Arrays;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = net.analizer.data.BuildConfig.class,
        sdk = Build.VERSION_CODES.N_MR1,
        application = NewsFeedApp.class
)
public class DataRepoTest extends BaseDataStoreTest {

    private DataRepo mDataRepo;
    private FeedItem mStory;

    @BeforeClass
    public static void initDAO() {
        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        mDataRepo = new DataRepoImp(daoSession);
        mStory = new FeedItem(16439270L, null, "mStory", "craigkerstiens", 1519320232L, "null", null, null, null, Arrays.asList(16440259L), "https://jacobian.org/writing/python-environment-2018/", 91, "My Python Development Environment, 2018 Edition", null, 47L);
    }

    @Test
    public void getNotExistedFeedItem_ShouldReturnOnlineFeed_And_SaveItIntoDatabase() {
        //inserted some stories
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<FeedItem> itemObserver = TestObserver.create();
        mDataRepo.getItemDetail(mStory.id).subscribe(itemObserver);
        itemObserver.awaitTerminalEvent();
        itemObserver.assertNoErrors();

        TestObserver<FeedItem> testOfflineObserver = TestObserver.create();
        offlineData.getItemDetail(mStory.id).subscribe(testOfflineObserver);
        testOfflineObserver.awaitTerminalEvent();
        testOfflineObserver.assertNoErrors();

        assertThat(testOfflineObserver.values()).isEqualTo(itemObserver.values());
    }

    @Test
    public void getTopStories_WhereDatabaseIsEmpty_ShouldReturnOnlineTopStories_And_SaveThemIntoDatabase() {

        TestObserver<FeedItem> testOnlineObserver = TestObserver.create();
        mDataRepo.getTopStories().subscribe(testOnlineObserver);
        testOnlineObserver.awaitTerminalEvent();
        testOnlineObserver.assertNoErrors();

        TestObserver<FeedItem> testOfflineObserver = TestObserver.create();
        offlineData.getTopStories().subscribe(testOfflineObserver);
        testOfflineObserver.awaitTerminalEvent();
        testOfflineObserver.assertNoErrors();

        testOnlineObserver.values().removeAll(testOfflineObserver.values());
        assertThat(testOnlineObserver.values().size()).isEqualTo(0);
    }

    @Test
    public void getGetComment_WhereCommentNotExistedInDatabase_ShouldReturnOnlineComment_And_SaveThemIntoDatabase() {
        //save an actual story with comment
        insertActualFeed();

        //check if data is inserted successfully
        checkIfActualFeedIsInsertedSuccessfully();

        //try to get the comment, this will eventually result in downloading the comment from API
        TestObserver<FeedItem> onlineCommentObserver = TestObserver.create();
        mDataRepo.getComment(mStory).subscribe(onlineCommentObserver);
        onlineCommentObserver.awaitTerminalEvent();
        onlineCommentObserver.assertNoErrors();

        //check if the comment is saved inside the local database
        TestObserver<FeedItem> offlineObserver = TestObserver.create();
        offlineData.getComment(mStory).subscribe(offlineObserver);
        offlineObserver.awaitTerminalEvent();
        offlineObserver.assertNoErrors();

        assertThat(offlineObserver.values()).isEqualTo(onlineCommentObserver.values());
    }

    @Test
    public void getCommentOfAStoryWithComment_ShouldReturnComment() {
        //inserted some stories
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<FeedItem> commentObserver = TestObserver.create();
        mDataRepo.getComment(storyWith1Comment).subscribe(commentObserver);
        commentObserver.awaitTerminalEvent();
        commentObserver.assertNoErrors();
        commentObserver.assertValue(existingComment);
    }

    @Test
    public void clearStories_WhereDatabaseIsEmpty_ShouldDoNothing() {
        TestObserver<Object> deleteObserver = TestObserver.create();
        mDataRepo.clearStories().subscribe(deleteObserver);
        deleteObserver.awaitTerminalEvent();
        deleteObserver.assertNoErrors();
        deleteObserver.assertNoValues();
    }

    @Test
    public void clearStories_WhereDatabaseIsPopulated_ShouldEmptyDatabase() {
        //inserted some stories
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<Object> deleteObserver = TestObserver.create();
        mDataRepo.clearStories().subscribe(deleteObserver);
        deleteObserver.awaitTerminalEvent();
        deleteObserver.assertNoErrors();
        deleteObserver.assertNoValues();
    }

    @Test
    public void clearCommentOfAStoryWithAComment_ShouldRemoveComment() {
        //inserted some stories
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<Object> deleteObserver = TestObserver.create();
        mDataRepo.clearComments(storyWith1Comment).subscribe(deleteObserver);
        deleteObserver.awaitTerminalEvent();
        deleteObserver.assertNoErrors();

        TestObserver<FeedItem> commentObserver = TestObserver.create();
        offlineData.getComment(storyWith1Comment).subscribe(commentObserver);
        commentObserver.awaitTerminalEvent();
        commentObserver.assertNoErrors();
        commentObserver.assertValueCount(0);
    }

    @Test
    public void clearCommentOfAStoryWithOutComments_ShouldDoNothing() {
        //inserted some stories
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<Object> deleteObserver = TestObserver.create();
        mDataRepo.clearComments(storyWithNoComment).subscribe(deleteObserver);
        deleteObserver.awaitTerminalEvent();
        deleteObserver.assertNoErrors();
    }

    private void insertActualFeed() {
        TestObserver<Object> insertObserver = TestObserver.create();
        Observable.fromArray(mStory)
                .concatMap(feedItem -> offlineData.save(feedItem))
                .subscribe(insertObserver);
        insertObserver.awaitTerminalEvent();
        insertObserver.assertNoErrors();
    }

    private void checkIfActualFeedIsInsertedSuccessfully() {
        TestObserver<FeedItem> checkInsertObserver = TestObserver.create();
        mDataRepo.getItemDetail(mStory.id).subscribe(checkInsertObserver);
        checkInsertObserver.awaitTerminalEvent();
        checkInsertObserver.assertNoErrors();
        checkInsertObserver.assertValueCount(1);
    }
}