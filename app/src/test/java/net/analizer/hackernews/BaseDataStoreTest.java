package net.analizer.hackernews;

import net.analizer.data.greenDao.DaoMaster;
import net.analizer.data.greenDao.DaoSession;
import net.analizer.data.greenDao.DbOpenHelper;
import net.analizer.data.models.OfflineDataStoreImp;
import net.analizer.domain.DataRepo;
import net.analizer.domain.DataStore;
import net.analizer.domain.models.FeedItem;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.TestObserver;

@SuppressWarnings("WeakerAccess")
public class BaseDataStoreTest {

    protected DaoSession daoSession;

    protected FeedItem storyWithNoComment;

    protected FeedItem storyWith1Comment;

    protected FeedItem anotherStoryWithNoComment;

    protected FeedItem notExistingStory;

    protected FeedItem existingComment;

    protected FeedItem notExistingComment;

    protected DataStore offlineData;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();

    @Before
    public void setUp() throws Exception {
        DbOpenHelper dbOpenHelper = new DbOpenHelper(RuntimeEnvironment.application, DataRepo.DB_NAME);

        daoSession = new DaoMaster(dbOpenHelper.getWritableDb())
                .newSession();

        List<Long> commentList = new ArrayList<>();
        commentList.add(3L);

        storyWithNoComment = new FeedItem(0L, false, FeedItem.TYPE_STORY, "by Me1", 1519292992L,
                "Test Content1", false, null, null, null, null, 0, "Title1", null, 0L);

        storyWith1Comment = new FeedItem(1L, false, FeedItem.TYPE_STORY, "by Me2", 1519297343L,
                "Test Content2", false, null, null, commentList, null, 0, "Title2", null, 0L);

        anotherStoryWithNoComment = new FeedItem(2L, false, FeedItem.TYPE_STORY, "by Me3", 1519299075L,
                "Test Content3", false, null, null, null, null, 0, "Title3", null, 0L);

        existingComment = new FeedItem(3L, false, FeedItem.TYPE_COMMENT, "by Me4", 1519301911L,
                "Test Content4", false, 1L, null, null, null, 0, "Title4", null, 0L);

        notExistingStory = new FeedItem(-1L, false, FeedItem.TYPE_STORY, "by Me5", 1519301912L,
                "Test Content5", false, null, null, null, null, 0, "Title5", null, 0L);

        notExistingComment = new FeedItem(-1L, false, FeedItem.TYPE_COMMENT, "by Me6", 1519301916L,
                "Test Content6", false, null, null, null, null, 0, "Title6", null, 0L);

        offlineData = new OfflineDataStoreImp(daoSession);

        clearStories();
        checkIfStoriesAreCleared();
    }

    @After
    public void tearDown() throws Exception {
        daoSession.getDatabase().close();
    }

    public void clearStories() {
        TestObserver testObserver = TestObserver.create();
        offlineData.deleteAllStories().subscribe(testObserver);
        testObserver.awaitTerminalEvent();
    }

    public void insertStories() {
        TestObserver testObserver = TestObserver.create();
        offlineData.save(storyWithNoComment)
                .concatWith(offlineData.save(storyWith1Comment))
                .concatWith(offlineData.save(existingComment))
                .concatWith(offlineData.save(anotherStoryWithNoComment))
                .subscribe(testObserver);

        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
    }

    public void checkIfStoriesAreInserted() {
        TestObserver<FeedItem> testObserver = TestObserver.create();

        offlineData.getTopStories().subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        testObserver.assertValues(storyWithNoComment, storyWith1Comment, anotherStoryWithNoComment);
    }

    public void checkIfStoriesAreCleared() {
        TestObserver<FeedItem> testObserver = TestObserver.create();
        offlineData.getTopStories().subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }
}