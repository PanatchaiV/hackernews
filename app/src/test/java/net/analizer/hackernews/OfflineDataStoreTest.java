package net.analizer.hackernews;

import android.os.Build;

import net.analizer.domain.models.FeedItem;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Arrays;

import edu.emory.mathcs.backport.java.util.Collections;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = net.analizer.data.BuildConfig.class,
        sdk = Build.VERSION_CODES.N_MR1,
        application = NewsFeedApp.class
)
public class OfflineDataStoreTest extends BaseDataStoreTest {

    @Test
    public void saveStories_DataStoreContainsStories() {
        insertStories();
        checkIfStoriesAreInserted();
    }

    @Test
    public void deleteAllStories_WhereDataStoreIsEmpty_ShouldEmptyDatabase() {
        //inserted some stories
        insertStories();
        checkIfStoriesAreInserted();

        //then try to clear DB
        clearStories();
        checkIfStoriesAreCleared();
    }

    @Test
    public void deleteFeeds_WhereDataStoreIsEmpty_ShouldNothingHappen() {
        //then try to clear DB
        clearStories();
        checkIfStoriesAreCleared();

        TestObserver<Object> objectTestObserver = TestObserver.create();
        offlineData.deleteFeedItem(Collections.singletonList(notExistingStory)).subscribe(objectTestObserver);
        objectTestObserver.awaitTerminalEvent();
        objectTestObserver.assertNoErrors();

        checkIfStoriesAreCleared();
    }

    @Test
    public void testInsertData() {
        FeedItem feedItem1 = new FeedItem(16439270L, null, "story", "craigkerstiens", 1519320232L, "null", null, null, null, Arrays.asList(16440259L), "https://jacobian.org/writing/python-environment-2018/", 91, "My Python Development Environment, 2018 Edition", null, 47L);
        FeedItem feedItem2 = new FeedItem(16414695L, null, "story", "jseliger", 1519065560L, "null", null, null, null, Arrays.asList(16415162L, 16415126L, 16416487L, 16415218L, 16415195L, 16415330L), "https://www.vox.com/energy-and-environment/2017/12/12/16767152/arctic-sea-ice-extent-chart", 62, "We’re witnessing the fastest decline in Arctic sea ice in at least 1,500 years", null, 26L);

        TestObserver<Object> testObserver = TestObserver.create();
        Observable.fromArray(feedItem1, feedItem2)
                .concatMap(feedItem -> offlineData.save(feedItem))
                .subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();

        TestObserver<Object> offlineObserver = TestObserver.create();
        offlineData.getTopStories().subscribe(offlineObserver);
        offlineObserver.awaitTerminalEvent();
        offlineObserver.assertNoErrors();

        testObserver.values().removeAll(offlineObserver.values());
        Assertions.assertThat(testObserver.values().size()).isEqualTo(0);
    }

    @Test
    public void queryTopStoriesFromEmptyDatabase_ReturnEmptyList() {

        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> topStories = offlineData.getTopStories();
        topStories.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }

    @Test
    public void queryASpecificFeedFromEmptyDatabase_ThrowError() {
        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = offlineData.getItemDetail(0L);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertError(Throwable.class);
    }

    @Test
    public void queryASpecificFeed_ReturnTheSpecifiedFeed() {
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = offlineData.getItemDetail(0L);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertValue(storyWithNoComment);
    }

    @Test
    public void queryCommentFromEmptyDatabase_ReturnEmptyCommentList() {
        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = offlineData.getComment(notExistingStory);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }

    @Test
    public void queryCommentFromNonExistingFeed_ReturnEmptyCommentList() {
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = offlineData.getComment(notExistingStory);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }

    @Test
    public void queryCommentFromFeedWithNoComments_ReturnEmptyCommentList() {
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = offlineData.getComment(storyWithNoComment);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }

    @Test
    public void queryCommentFromFeedWithComments_ReturnCommentList() {
        insertStories();
        checkIfStoriesAreInserted();

        TestObserver<FeedItem> testObserver = TestObserver.create();
        Observable<FeedItem> feedItemObservable = offlineData.getComment(storyWith1Comment);
        feedItemObservable.subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertValues(existingComment);
    }
}