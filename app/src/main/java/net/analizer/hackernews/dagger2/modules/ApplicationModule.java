package net.analizer.hackernews.dagger2.modules;

import net.analizer.data.greenDao.DaoSession;
import net.analizer.data.models.DataRepoImp;
import net.analizer.domain.DataRepo;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private DaoSession mDaoSession;

    public ApplicationModule(DaoSession daoSession) {
        this.mDaoSession = daoSession;
    }

    @Provides
    @Singleton
    public DaoSession provideDaoSession() {
        return mDaoSession;
    }

    @Provides
    @Singleton
    public DataRepo provideDataRepo(DaoSession daoSession) {
        return new DataRepoImp(daoSession);
    }
}
