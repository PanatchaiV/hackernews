package net.analizer.hackernews.dagger2;

public interface HasComponent<T> {
    T getComponent();
}