package net.analizer.hackernews.dagger2.modules;

import net.analizer.hackernews.mvp.detail.DetailPresenterImpl;
import net.analizer.hackernews.mvp.detail.DetailScreen;
import net.analizer.hackernews.mvp.list.ListScreen;
import net.analizer.hackernews.mvp.list.ListScreenPresenterImpl;
import net.analizer.hackernews.mvp.splash.SplashScreen;
import net.analizer.hackernews.mvp.splash.SplashScreenPresenterImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MvpModule {

    @Binds
    public abstract SplashScreen.SplashScreenPresenter provideSplashScreenPresenter(SplashScreenPresenterImpl splashScreenPresenter);

    @Binds
    public abstract ListScreen.ListScreenPresenter provideListScreenPresenter(ListScreenPresenterImpl listScreenPresenter);

    @Binds
    public abstract DetailScreen.DetailPresenter provideDetailPresenter(DetailPresenterImpl detailPresenter);

}
