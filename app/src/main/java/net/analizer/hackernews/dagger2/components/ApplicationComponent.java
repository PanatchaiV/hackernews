package net.analizer.hackernews.dagger2.components;

import android.content.Context;

import net.analizer.hackernews.FeedDetailActivity;
import net.analizer.hackernews.NewsFeedActivity;
import net.analizer.hackernews.NewsFeedApp;
import net.analizer.hackernews.SplashActivity;
import net.analizer.hackernews.dagger2.modules.ApplicationModule;
import net.analizer.hackernews.dagger2.modules.MvpModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                MvpModule.class
        }
)
public interface ApplicationComponent {

    void inject(NewsFeedActivity newsFeedActivity);

    void inject(SplashActivity splashActivity);

    void inject(FeedDetailActivity feedDetailActivity);
    //inject more Activities

    //Exposed to sub-graphs.
    Context applicationContext();

    NewsFeedApp application();

    @Component.Builder
    interface Builder {

        ApplicationComponent build();

        @BindsInstance
        Builder application(NewsFeedApp application);

        @BindsInstance
        Builder context(Context context);

        Builder appModule(ApplicationModule netModule);
    }
}
