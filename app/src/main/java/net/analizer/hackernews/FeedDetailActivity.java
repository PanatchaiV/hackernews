package net.analizer.hackernews;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import net.analizer.domain.models.FeedItem;
import net.analizer.hackernews.dagger2.components.ApplicationComponent;
import net.analizer.hackernews.databinding.ActivityFeedDetailBinding;
import net.analizer.hackernews.mvp.detail.DetailScreen;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Sadly I do have much time left to do everything properly
 */
public class FeedDetailActivity extends BaseActivity implements DetailScreen.DetailView {

    public static final String EXTRA_FEED = "EXTRA_FEED";
    public static final String EXTRA_COMMENT_LIST = "EXTRA_COMMENT_LIST";

    @Inject
    DetailScreen.DetailPresenter detailPresenter;

    private ActivityFeedDetailBinding mViewBinding;
    private FeedItem mFeedItem;

    private CommentListAdapter mAdapter;

    public static Intent getIntent(AppCompatActivity activity, FeedItem feedItem) {
        Intent intent = new Intent(activity, FeedDetailActivity.class);
        Parcelable wrap = Parcels.wrap(feedItem);
        intent.putExtra(EXTRA_FEED, wrap);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
        initViews();

        List<FeedItem> commentList;
        if (savedInstanceState != null) {
            Parcelable feed = savedInstanceState.getParcelable(EXTRA_FEED);
            Parcelable comment = savedInstanceState.getParcelable(EXTRA_COMMENT_LIST);
            mFeedItem = Parcels.unwrap(feed);
            commentList = Parcels.unwrap(comment);

        } else {
            Intent intent = getIntent();
            Parcelable parcelableExtra = intent.getParcelableExtra(EXTRA_FEED);
            mFeedItem = Parcels.unwrap(parcelableExtra);
            commentList = new ArrayList<>();
        }

        mAdapter = new CommentListAdapter(commentList);
        mViewBinding.commentRecyclerView.setAdapter(mAdapter);
        mAdapter.displayLoading();

        detailPresenter.startLoadingFeedComment(mFeedItem);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Parcelable feed = Parcels.wrap(mFeedItem);
        Parcelable comment = Parcels.wrap(mAdapter.getCommentList());
        outState.putParcelable(EXTRA_FEED, feed);
        outState.putParcelable(EXTRA_COMMENT_LIST, comment);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detailPresenter.stopLoadingFeedComment();
    }

    private void initializeInjector() {
        ApplicationComponent applicationComponent = getApplicationComponent();
        applicationComponent.inject(this);
    }

    private void initViews() {
        mViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_feed_detail);
        detailPresenter.setView(this);

        mViewBinding.scrollToTopFAB.setOnClickListener(
                v -> {
                    mViewBinding.commentRecyclerView.scrollToPosition(0);
                    mViewBinding.feedAppbar.setExpanded(true);
                    mViewBinding.scrollToTopFAB.hide();
                });

        mViewBinding.commentRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 10) {
                    mViewBinding.scrollToTopFAB.hide();

                } else if (dy < 10) {
                    mViewBinding.scrollToTopFAB.show();
                }
            }
        });
        mViewBinding.scrollToTopFAB.hide();
    }

    @Override
    public void noMoreData() {
        mAdapter.hideLoading();
    }

    @Override
    public void displayFeed(FeedItem feedItem) {
        if (feedItem.title != null && feedItem.title.length() > 0) {
            mViewBinding.titleTextView.setText(Html.fromHtml(feedItem.title));
        } else {

            mViewBinding.titleTextView.setText("No Title");
        }

        if (feedItem.text != null && feedItem.text.length() > 0) {
            mViewBinding.msgTextView.setText(Html.fromHtml(feedItem.text));
            mViewBinding.msgTextView.setVisibility(View.VISIBLE);

        } else {
            mViewBinding.msgTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayComment(FeedItem comment) {
        mAdapter.addComment(comment);
    }

    @Override
    public void displayError(String msg) {
        Toast.makeText(FeedDetailActivity.this, msg, Toast.LENGTH_LONG).show();
        finish();
    }

    private static class CommentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int VIEW_TYPE_COMMENT = 0;
        private static final int VIEW_TYPE_PROGRESS = 1;

        private List<FeedItem> mCommentList;
        private int mLastPositionDisplayed = -1;
        private boolean mIsLoading = false;

        public CommentListAdapter(List<FeedItem> commentList) {
            mCommentList = commentList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_COMMENT) {
                return inflateFeed(parent);
            }

            return inflateProgress(parent);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            int viewType = getItemViewType(position);
            if (viewType == VIEW_TYPE_COMMENT) {
                bindFeed((FeedHolder) holder, position);

            } else {
                bindProgress((ProgressHolder) holder, position);
            }
        }

        @Override
        public int getItemCount() {
            return mCommentList != null ? mCommentList.size() : 0;
        }

        @Override
        public int getItemViewType(int position) {

            int itemCount = getItemCount();
            if (position == itemCount - 1 && mIsLoading) {
                return VIEW_TYPE_PROGRESS;
            }

            return VIEW_TYPE_COMMENT;
        }

        void displayLoading() {
            mIsLoading = true;
            int positionToBeInsertedInto = getItemCount();
            notifyItemInserted(positionToBeInsertedInto);
        }

        void hideLoading() {
            int positionToBeRemovedFrom = getItemCount();
            notifyItemRemoved(positionToBeRemovedFrom);
            mIsLoading = false;
            notifyItemRangeChanged(positionToBeRemovedFrom, getItemCount());
        }

        void addComment(FeedItem comment) {
            int itemPositionToBeInsertedInto = getItemCount();
            mCommentList.add(comment);
            notifyItemInserted(itemPositionToBeInsertedInto);
        }

        List<FeedItem> getCommentList() {
            return new ArrayList<>(mCommentList);
        }

        private FeedHolder inflateFeed(ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

            View view = layoutInflater.inflate(R.layout.layout_feed, parent, false);
            return new FeedHolder(view);
        }

        private ProgressHolder inflateProgress(ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.layout_progress, parent, false);
            return new ProgressHolder(view);
        }

        private void bindFeed(FeedHolder holder, int position) {
            FeedItem feedItem = mCommentList.get(position);
            if (feedItem.by != null && feedItem.by.length() > 0) {
                holder.title.setText(Html.fromHtml(feedItem.by));
            } else {

                holder.title.setText("Unknown");
            }

            if (feedItem.text != null && feedItem.text.length() > 0) {
                holder.content.setText(Html.fromHtml(feedItem.text));

            } else {
                holder.content.setText("");
            }

            setAnimation(holder.itemView, position);
        }

        private void bindProgress(ProgressHolder holder, int position) {
            setAnimation(holder.itemView, position);
        }

        private void setAnimation(View viewToAnimate, int position) {
            if (position > mLastPositionDisplayed) {
                Animation animation = AnimationUtils.loadAnimation(
                        viewToAnimate.getContext(),
                        android.R.anim.fade_in);

                viewToAnimate.startAnimation(animation);
                mLastPositionDisplayed = position;
            }
        }
    }

    private static class FeedHolder extends RecyclerView.ViewHolder {

        LinearLayout container;
        TextView title;
        TextView content;

        public FeedHolder(View itemView) {
            super(itemView);

            container = (LinearLayout) itemView.findViewById(R.id.feedContainer_linearLayout);
            title = (TextView) itemView.findViewById(R.id.title_textView);
            content = (TextView) itemView.findViewById(R.id.msg_textView);
        }
    }

    private static class ProgressHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public ProgressHolder(View itemView) {
            super(itemView);

            progressBar = (ProgressBar) itemView.findViewById(R.id.loading_progressBar);
        }
    }
}
