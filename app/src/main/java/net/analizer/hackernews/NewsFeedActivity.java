package net.analizer.hackernews;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import net.analizer.domain.models.FeedItem;
import net.analizer.hackernews.dagger2.components.ApplicationComponent;
import net.analizer.hackernews.databinding.ActivityNewsFeedBinding;
import net.analizer.hackernews.mvp.list.ListScreen;
import net.analizer.hackernews.mvp.list.OnFeedClickListener;

import org.parceler.Parcels;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class NewsFeedActivity extends BaseActivity
        implements ListScreen.ListScreenView, OnFeedClickListener {

    public static final String EXTRA_FEED = "EXTRA_FEED";

    @Inject
    ListScreen.ListScreenPresenter listScreenPresenter;

    private ActivityNewsFeedBinding mViewBinding;

    private FeedListAdapter mAdapter;

    public static Intent getIntent(AppCompatActivity activity, List<FeedItem> feedList) {
        Intent intent = new Intent(activity, NewsFeedActivity.class);
        Parcelable wrap = Parcels.wrap(feedList);
        intent.putExtra(EXTRA_FEED, wrap);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
        initViews();

        List<FeedItem> feedList;
        if (savedInstanceState != null) {
            Parcelable parcelable = savedInstanceState.getParcelable(EXTRA_FEED);
            feedList = Parcels.unwrap(parcelable);

            mAdapter = new FeedListAdapter(feedList, NewsFeedActivity.this);
            mViewBinding.feedRecyclerView.setAdapter(mAdapter);

        } else {
            feedList = new ArrayList<>();
            mAdapter = new FeedListAdapter(feedList, NewsFeedActivity.this);
            mViewBinding.feedRecyclerView.setAdapter(mAdapter);
            mAdapter.displayLoading();
            listScreenPresenter.startLoadingFeed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Parcelable wrap = Parcels.wrap(mAdapter.getFeedList());
        outState.putParcelable(EXTRA_FEED, wrap);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listScreenPresenter.stopLoadingFeed();
    }

    private void initializeInjector() {
        ApplicationComponent applicationComponent = getApplicationComponent();
        applicationComponent.inject(this);
    }

    @Override
    public void noMoreData() {
        mAdapter.hideLoading();
    }

    @Override
    public void displayFeed(@NonNull FeedItem feedItem) {
        mAdapter.addFeed(feedItem);
    }

    @Override
    public void displayFeedDetail(FeedItem feedItem) {
        Intent intent = FeedDetailActivity.getIntent(NewsFeedActivity.this, feedItem);
        startActivity(intent);
    }

    @Override
    public void displayError(Throwable t) {
        Toast.makeText(NewsFeedActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClicked(FeedItem feedItem) {
        listScreenPresenter.onFeedClick(feedItem);
    }

    private void initViews() {
        mViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_news_feed);
        listScreenPresenter.setView(this);

        mViewBinding.scrollToTopFAB.setOnClickListener(
                v -> {
                    mViewBinding.feedRecyclerView.smoothScrollToPosition(0);
                    mViewBinding.scrollToTopFAB.hide();
                });

        mViewBinding.feedRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 10) {
                    mViewBinding.scrollToTopFAB.hide();

                } else if (dy < 10) {
                    mViewBinding.scrollToTopFAB.show();
                }
            }
        });
        mViewBinding.scrollToTopFAB.hide();

        mViewBinding.swipeRefresh.setOnRefreshListener(() -> {
            listScreenPresenter.stopLoadingFeed();
            mAdapter.hideLoading();
            mAdapter.clear();

            new Handler().postDelayed(
                    () -> {
                        mAdapter.displayLoading();
                        listScreenPresenter.reloadFeedList();
                        mViewBinding.swipeRefresh.setRefreshing(false);
                    }, 1000);
        });
    }

    private static class FeedListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int VIEW_TYPE_FEED = 0;
        private static final int VIEW_TYPE_PROGRESS = 1;

        private WeakReference<OnFeedClickListener> mListenerRef;
        private List<FeedItem> mFeedList;
        private int mLastPositionDisplayed = -1;
        private boolean mIsLoading = false;

        public FeedListAdapter(List<FeedItem> feedList,
                               OnFeedClickListener listener) {

            mListenerRef = new WeakReference<>(listener);
            mFeedList = feedList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            if (viewType == VIEW_TYPE_FEED) {
                return inflateFeed(parent);
            }

            return inflateProgress(parent);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            int viewType = getItemViewType(position);
            if (viewType == VIEW_TYPE_FEED) {
                bindFeed((FeedHolder) holder, position);

            } else {
                bindProgress((ProgressHolder) holder, position);
            }
        }

        @Override
        public int getItemViewType(int position) {

            int itemCount = getItemCount();
            if (position == itemCount - 1 && mIsLoading) {
                return VIEW_TYPE_PROGRESS;
            }

            return VIEW_TYPE_FEED;
        }

        @Override
        public int getItemCount() {
            return mFeedList != null ? mFeedList.size() + (mIsLoading ? 1 : 0) : 0;
        }

        void addFeed(FeedItem feedItem) {
            int positionToBeInsertedInto = getItemCount();
            mFeedList.add(feedItem);
            notifyItemInserted(positionToBeInsertedInto);
        }

        List<FeedItem> getFeedList() {
            return new ArrayList<>(mFeedList);
        }

        void displayLoading() {
            mIsLoading = true;
            int positionToBeInsertedInto = getItemCount();
            notifyItemInserted(positionToBeInsertedInto);
        }

        void hideLoading() {
            int positionToBeRemovedFrom = getItemCount();
            notifyItemRemoved(positionToBeRemovedFrom);
            mIsLoading = false;
            notifyItemRangeChanged(positionToBeRemovedFrom, getItemCount());
        }

        void clear() {
            mIsLoading = false;
            mFeedList.clear();
            notifyDataSetChanged();
        }

        private FeedHolder inflateFeed(ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

            View view = layoutInflater.inflate(R.layout.layout_feed, parent, false);

            FeedHolder feedHolder = new FeedHolder(view);
            feedHolder.container.setOnClickListener(
                    v -> {
                        OnFeedClickListener listener = mListenerRef.get();
                        if (listener != null) {
                            listener.onClicked((FeedItem) v.getTag());
                        }
                    });

            return feedHolder;
        }

        private ProgressHolder inflateProgress(ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.layout_progress, parent, false);
            return new ProgressHolder(view);
        }

        private void bindFeed(FeedHolder holder, int position) {
            FeedItem feedItem = mFeedList.get(position);
            holder.container.setTag(feedItem);

            if (feedItem.title != null && feedItem.title.length() > 0) {
                holder.title.setText(Html.fromHtml(feedItem.title));
            } else {

                holder.title.setText("No Title");
            }

            if (feedItem.text != null && feedItem.text.length() > 0) {
                holder.content.setText(Html.fromHtml(feedItem.text));
                holder.content.setVisibility(View.VISIBLE);

            } else {
                holder.content.setVisibility(View.GONE);
            }

            setAnimation(holder.itemView, position);
        }

        private void bindProgress(ProgressHolder holder, int position) {
            setAnimation(holder.itemView, position);
        }

        private void setAnimation(View viewToAnimate, int position) {
            if (position > mLastPositionDisplayed) {
                Animation animation = AnimationUtils.loadAnimation(
                        viewToAnimate.getContext(),
                        android.R.anim.fade_in);

                viewToAnimate.startAnimation(animation);
                mLastPositionDisplayed = position;
            }
        }
    }

    private static class FeedHolder extends RecyclerView.ViewHolder {

        LinearLayout container;
        TextView title;
        TextView content;

        public FeedHolder(View itemView) {
            super(itemView);

            container = (LinearLayout) itemView.findViewById(R.id.feedContainer_linearLayout);
            title = (TextView) itemView.findViewById(R.id.title_textView);
            content = (TextView) itemView.findViewById(R.id.msg_textView);
        }
    }

    private static class ProgressHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public ProgressHolder(View itemView) {
            super(itemView);

            progressBar = (ProgressBar) itemView.findViewById(R.id.loading_progressBar);
        }
    }
}
