package net.analizer.hackernews;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import net.analizer.domain.models.FeedItem;
import net.analizer.hackernews.dagger2.components.ApplicationComponent;
import net.analizer.hackernews.databinding.ActivitySplashBinding;
import net.analizer.hackernews.mvp.splash.SplashScreen;

import java.util.List;

import javax.inject.Inject;

/**
 * Sadly I do not have time for MVVP so MVP has to do the job
 */
public class SplashActivity extends BaseActivity implements SplashScreen.SplashScreenView {

    @Inject
    SplashScreen.SplashScreenPresenter splashScreenPresenter;

    private ActivitySplashBinding mViewBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
        initViews();
        initFeeds();
    }

    private void initializeInjector() {
        ApplicationComponent applicationComponent = getApplicationComponent();
        applicationComponent.inject(this);
    }

    private void initViews() {
        mViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        splashScreenPresenter.setView(this);
    }

    private void initFeeds() {
        splashScreenPresenter.onStartLoading();
    }

    @Override
    public void showLoading() {
        mViewBinding.loadingProgressProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoading() {
        mViewBinding.loadingProgressProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showNextView(@NonNull List<FeedItem> feedList) {
        Intent intent = NewsFeedActivity.getIntent(SplashActivity.this, feedList);
        startActivity(intent);
        finish();
    }

    @Override
    public void showMessage(@NonNull String msg) {
        mViewBinding.splashMsgTextView.setText(msg);
    }
}
