package net.analizer.hackernews.mvp.list;

import android.support.annotation.NonNull;

import net.analizer.domain.DataRepo;
import net.analizer.domain.models.FeedItem;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ListScreenPresenterImpl implements ListScreen.ListScreenPresenter {

    private DataRepo mFeedRepo;
    private ListScreen.ListScreenView mView;
    private Disposable mFeedLoader;

    @Inject
    public ListScreenPresenterImpl(DataRepo feedRepo) {
        mFeedRepo = feedRepo;
    }

    @Override
    public void setView(@NonNull ListScreen.ListScreenView view) {
        mView = view;
    }

    @Override
    public void startLoadingFeed() {
        if (mView != null) {
            mFeedRepo.getTopStories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<FeedItem>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            mFeedLoader = d;
                        }

                        @Override
                        public void onNext(FeedItem feedItem) {
                            if (mView != null) {
                                mView.displayFeed(feedItem);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (mView != null) {
                                mView.noMoreData();
                                mView.displayError(e);
                            }
                            mFeedLoader = null;
                        }

                        @Override
                        public void onComplete() {
                            if (mView != null) {
                                mView.noMoreData();
                            }
                            mFeedLoader = null;
                        }
                    });
        }
    }

    @Override
    public void stopLoadingFeed() {
        if (mFeedLoader != null) {
            mFeedLoader.dispose();
            mFeedLoader = null;
        }
    }

    @Override
    public void onFeedClick(@NonNull FeedItem feedItem) {
        if (mView != null) {
            mView.displayFeedDetail(feedItem);
        }
    }

    @Override
    public void reloadFeedList() {
        if (mView != null) {
            mFeedRepo.reloadStories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<FeedItem>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            mFeedLoader = d;
                        }

                        @Override
                        public void onNext(FeedItem feedItem) {
                            if (mView != null && feedItem.id != null) {
                                mView.displayFeed(feedItem);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (mView != null) {
                                mView.noMoreData();
                                mView.displayError(e);
                            }
                            mFeedLoader = null;
                        }

                        @Override
                        public void onComplete() {
                            if (mView != null) {
                                mView.noMoreData();
                            }
                            mFeedLoader = null;
                        }
                    });
        }
    }
}
