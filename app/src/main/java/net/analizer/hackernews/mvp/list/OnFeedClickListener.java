package net.analizer.hackernews.mvp.list;

import net.analizer.domain.models.FeedItem;

public interface OnFeedClickListener {
    /**
     * Invoked on Feed clicked
     */
    void onClicked(FeedItem feedItem);
}
