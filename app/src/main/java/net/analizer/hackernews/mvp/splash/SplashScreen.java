package net.analizer.hackernews.mvp.splash;

import android.support.annotation.NonNull;

import net.analizer.domain.models.FeedItem;

import java.util.List;

/**
 * No time for MVVM sorry
 */
public abstract class SplashScreen {
    public interface SplashScreenView {
        void showLoading();

        void dismissLoading();

        void showNextView(@NonNull List<FeedItem> feedList);

        void showMessage(@NonNull String msg);
    }

    public interface SplashScreenPresenter {

        void setView(@NonNull SplashScreenView view);

        void onStartLoading();
    }
}
