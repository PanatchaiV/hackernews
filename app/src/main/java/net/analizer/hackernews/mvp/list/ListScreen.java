package net.analizer.hackernews.mvp.list;

import android.support.annotation.NonNull;

import net.analizer.domain.models.FeedItem;

/**
 * No time for MVVM sorry
 */
public abstract class ListScreen {
    public interface ListScreenView {

        void noMoreData();

        void displayFeed(@NonNull FeedItem feedItem);

        void displayFeedDetail(FeedItem feedItem);

        void displayError(Throwable t);
    }

    public interface ListScreenPresenter {

        void setView(@NonNull ListScreenView view);

        void startLoadingFeed();

        void stopLoadingFeed();

        void onFeedClick(@NonNull FeedItem feedItem);

        void reloadFeedList();
    }
}
