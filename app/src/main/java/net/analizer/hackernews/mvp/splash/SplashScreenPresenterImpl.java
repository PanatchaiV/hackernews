package net.analizer.hackernews.mvp.splash;

import android.support.annotation.NonNull;
import android.util.Log;

import net.analizer.domain.DataRepo;
import net.analizer.domain.models.FeedItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SplashScreenPresenterImpl implements SplashScreen.SplashScreenPresenter {

    private static final String TAG = SplashScreenPresenterImpl.class.getSimpleName();

    private List<FeedItem> mFeedItemList;
    private SplashScreen.SplashScreenView mView;
    private DataRepo mFeedRepo;

    @Inject
    public SplashScreenPresenterImpl(DataRepo feedRepo) {
        mFeedRepo = feedRepo;
        mFeedItemList = new ArrayList<>();
    }

    @Override
    public void setView(@NonNull SplashScreen.SplashScreenView view) {
        mView = view;
    }

    @Override
    public void onStartLoading() {
        if (mView != null) {
            mView.showLoading();

            mFeedRepo.getTopStories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<FeedItem>() {

                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(FeedItem feedItem) {
                            Log.d(TAG, "onNext: " + feedItem.toString());
                            onFeedLoaded(feedItem);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "onError: ", e);
                            onLoadError(e);
                        }

                        @Override
                        public void onComplete() {
                            onFinishLoading();
                        }
                    });
        }
    }

    private void onFinishLoading() {
        if (mView != null) {
            mView.dismissLoading();
            mView.showNextView(mFeedItemList);
        }
    }

    private void onFeedLoaded(@NonNull FeedItem feedItem) {
        mFeedItemList.add(feedItem);

        if (mView != null) {
            mView.showMessage(String.format("Loaded: %s", feedItem.title));
        }
    }

    private void onLoadError(@NonNull Throwable t) {
        if (mView != null) {
            mView.dismissLoading();
            mView.showMessage(String.format("Error: %s", t));
        }
    }
}
