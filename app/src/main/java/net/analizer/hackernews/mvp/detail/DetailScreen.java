package net.analizer.hackernews.mvp.detail;

import net.analizer.domain.models.FeedItem;

import java.util.List;

public abstract class DetailScreen {
    public interface DetailView {

        void noMoreData();

        void displayFeed(FeedItem feedItem);

        void displayComment(FeedItem comment);

        void displayError(String msg);
    }

    public interface DetailPresenter {
        void setView(DetailView view);

        void startLoadingFeedComment(FeedItem feedItem);

        void stopLoadingFeedComment();
    }
}
