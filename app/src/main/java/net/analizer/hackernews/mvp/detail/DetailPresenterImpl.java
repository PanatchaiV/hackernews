package net.analizer.hackernews.mvp.detail;

import net.analizer.domain.DataRepo;
import net.analizer.domain.models.FeedItem;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DetailPresenterImpl implements DetailScreen.DetailPresenter {

    private DataRepo mFeedRepo;
    private DetailScreen.DetailView mDetailView;
    private Disposable mCommentLoader;

    @Inject
    public DetailPresenterImpl(DataRepo feedRepo) {
        mFeedRepo = feedRepo;
    }

    @Override
    public void setView(DetailScreen.DetailView view) {
        mDetailView = view;
    }

    @Override
    public void startLoadingFeedComment(FeedItem feedItem) {
        if (mDetailView != null) {
            mDetailView.displayFeed(feedItem);

            mFeedRepo.getComment(feedItem)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<FeedItem>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            mCommentLoader = d;
                        }

                        @Override
                        public void onNext(FeedItem feedItem) {
                            if (mDetailView != null) {
                                mDetailView.displayComment(feedItem);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (mDetailView != null) {
                                mDetailView.noMoreData();
                                mDetailView.displayError(e.getMessage());
                            }
                            mCommentLoader = null;
                        }

                        @Override
                        public void onComplete() {
                            if (mDetailView != null) {
                                mDetailView.noMoreData();
                            }
                            mCommentLoader = null;
                        }
                    });
        }
    }

    @Override
    public void stopLoadingFeedComment() {
        if (mCommentLoader != null) {
            mCommentLoader.dispose();
            mCommentLoader = null;
        }
    }
}
