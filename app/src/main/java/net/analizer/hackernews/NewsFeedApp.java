package net.analizer.hackernews;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import net.analizer.data.greenDao.DaoMaster;
import net.analizer.data.greenDao.DaoSession;
import net.analizer.data.greenDao.DbOpenHelper;
import net.analizer.domain.DataRepo;
import net.analizer.hackernews.dagger2.HasComponent;
import net.analizer.hackernews.dagger2.components.ApplicationComponent;
import net.analizer.hackernews.dagger2.components.DaggerApplicationComponent;
import net.analizer.hackernews.dagger2.modules.ApplicationModule;

public class NewsFeedApp extends Application
        implements HasComponent<ApplicationComponent> {

    private ApplicationComponent mApplicationComponent;
    private ApplicationModule mApplicationModule;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeDependency();
    }

    private void initializeDependency() {
        DbOpenHelper dbOpenHelper = new DbOpenHelper(this, DataRepo.DB_NAME);
        DaoSession daoSession = new DaoMaster(dbOpenHelper.getWritableDb())
                .newSession();

        mApplicationModule = new ApplicationModule(daoSession);
        setAppModule(mApplicationModule);
    }

    @VisibleForTesting
    public void setAppModule(ApplicationModule module) {
        mApplicationModule = module;
        mApplicationComponent = DaggerApplicationComponent.builder()
                .application(this)
                .context(getBaseContext())
                .appModule(mApplicationModule)
                .build();
    }

    @Override
    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }
}
