package net.analizer.hackernews;

import android.support.v7.app.AppCompatActivity;

import net.analizer.hackernews.dagger2.HasComponent;
import net.analizer.hackernews.dagger2.components.ApplicationComponent;

public abstract class BaseActivity extends AppCompatActivity {

    /**
     * Get the Main Application component for dependency injection.
     *
     * @return {@link ApplicationComponent}
     */
    @SuppressWarnings("unchecked")
    protected ApplicationComponent getApplicationComponent() {
        return ((HasComponent<ApplicationComponent>) getApplication()).getComponent();
    }
}
