package net.analizer.domain;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.analizer.domain.models.FeedItem;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface DataStore {
    /**
     * get top stories
     *
     * @return top stories if found. Throw error otherwise.
     */
    Observable<FeedItem> getTopStories();

    /**
     * Get a FeedItem detail
     *
     * @param itemId item id
     * @return Detail of the FeedItem if found. Throw error otherwise.
     */
    Observable<FeedItem> getItemDetail(@NonNull Long itemId);

    /**
     * get comments for the given story
     *
     * @param story story
     * @return all comments if found. Throw error otherwise.
     */
    Observable<FeedItem> getComment(@NonNull FeedItem story);

    /**
     * Save FeedItem into a local storage.
     *
     * @param FeedItem feed item
     */
    Observable<FeedItem> save(@NonNull FeedItem FeedItem);

    /**
     * Delete all feed items from the local storage
     */
    Completable deleteAllStories();

    /**
     * Delete the given feed list from the local storage
     *
     * @param FeedItemList feeds to be deleted. If null, delete all.
     */
    Completable deleteFeedItem(@Nullable List<FeedItem> FeedItemList);
}
