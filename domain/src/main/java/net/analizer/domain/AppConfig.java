package net.analizer.domain;

public class AppConfig {

    // if we have variants, this could point to different end points
    public static final String BASE_DOMAIN = "https://hacker-news.firebaseio.com/v0/";
}
