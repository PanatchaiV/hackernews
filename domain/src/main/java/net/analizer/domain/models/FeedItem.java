package net.analizer.domain.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class FeedItem {
    public static final String TYPE_STORY = "story";
    public static final String TYPE_COMMENT = "comment";
    public static final String TYPE_JOB = "job";
    public static final String TYPE_POLL = "poll";
    public static final String TYPE_pollopt = "pollopt";

    @SerializedName("id")
    public Long id;//  The item's unique id.

    @SerializedName("deleted")
    public Boolean deleted;//  true if the item is deleted.

    @SerializedName("type")
    public String type;//  The type of item. One of "job", "story", "comment", "poll", or "pollopt".

    @SerializedName("by")
    public String by;//  The username of the item's author.

    @SerializedName("time")
    public Long time;//  Creation date of the item, in Unix Time.

    @SerializedName("text")
    public String text;//  The comment, story or poll text. HTML.

    @SerializedName("dead")
    public Boolean dead;//  true if the item is dead.

    @SerializedName("parent")
    public Long parent;//  The comment's parent: either another comment or the relevant story.

    @SerializedName("poll")
    public Long poll;//  The pollopt's associated poll.

    @SerializedName("kids")
    public List<Long> kids;//  The ids of the item's comments, in ranked display order.

    @SerializedName("url")
    public String url;//  The URL of the story.

    @SerializedName("score")
    public Integer score;//  The story's score, or the votes for a pollopt.

    @SerializedName("title")
    public String title;//  The title of the story, poll or job.

    @SerializedName("parts")
    public List<Long> parts;//  A list of related pollopts, in display order.

    @SerializedName("descendants")
    public Long descendants;//  In the case of stories or polls, the total comment count.

    public FeedItem() {
    }

    public FeedItem(Long id) {
        this.id = id;
    }

    public FeedItem(Long id, Boolean deleted, String type, String by, Long time, String text,
                    Boolean dead, Long parent, Long poll, List<Long> kids, String url,
                    Integer score, String title, List<Long> parts, Long descendants) {
        this.id = id;
        this.deleted = deleted;
        this.type = type;
        this.by = by;
        this.time = time;
        this.text = text;
        this.dead = dead;
        this.parent = parent;
        this.poll = poll;
        this.kids = kids;
        this.url = url;
        this.score = score;
        this.title = title;
        this.parts = parts;
        this.descendants = descendants;
    }

    public Long getItemId() {
        return id;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public String getItemType() {
        return type;
    }

    public String getUserName() {
        return by;
    }

    public Long getCreationTime() {
        return time;
    }

    public String getContent() {
        return text;
    }

    public Boolean isDead() {
        return dead;
    }

    public Long getParentId() {
        return parent;
    }

    public Long getPollId() {
        return poll;
    }

    public List<Long> getChildItemList() {
        return kids;
    }

    public String getUrl() {
        return url;
    }

    public Integer getScore() {
        return score;
    }

    public String getTitle() {
        return title;
    }

    public List<Long> getRelatedPollList() {
        return parts;
    }

    public Long getCommentCount() {
        return descendants;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FeedItem) {
            FeedItem feedItem = (FeedItem) obj;
            return feedItem.id != null && feedItem.id.equals(id);
        }

        return false;
    }

    @Override
    public String toString() {
        return "FeedItem{" +
                "id=" + id +
                ", deleted=" + deleted +
                ", type='" + type + '\'' +
                ", by='" + by + '\'' +
                ", time=" + time +
                ", text='" + text + '\'' +
                ", dead=" + dead +
                ", parent=" + parent +
                ", poll=" + poll +
                ", kids=" + kids +
                ", url='" + url + '\'' +
                ", score=" + score +
                ", title='" + title + '\'' +
                ", parts=" + parts +
                ", descendants=" + descendants +
                '}';
    }
}
