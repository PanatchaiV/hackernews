package net.analizer.domain;

import android.support.annotation.NonNull;

import net.analizer.domain.models.FeedItem;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface DataRepo {
    String DB_NAME = "hacker_news_v1_001.db";

    /**
     * Get a specific item detail
     *
     * @param itemId item id
     * @return {@link FeedItem}
     */
    Observable<FeedItem> getItemDetail(Long itemId);

    /**
     * get top stories
     *
     * @return top stories if found. Throw error otherwise.
     */
    Observable<FeedItem> getTopStories();

    /**
     * get comments for the given story
     *
     * @param story story id
     * @return all comments if found. Throw error otherwise.
     */
    Observable<FeedItem> getComment(@NonNull FeedItem story);

    /**
     * Reload all stories if found. Throw error otherwise.
     */
    Observable<FeedItem> reloadStories();

    /**
     * Reload all comment if found. Throw error otherwise.
     */
    Observable<FeedItem> reloadComments(@NonNull FeedItem story);

    /**
     * Remove all stories from the local storage
     */
    Completable clearStories();

    /**
     * Remove all comments from the local storage
     *
     * @param story Remove all comments of that particular story.
     */
    Completable clearComments(@NonNull FeedItem story);
}
