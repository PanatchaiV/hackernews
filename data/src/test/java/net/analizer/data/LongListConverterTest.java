package net.analizer.data;

import net.analizer.data.greenDao.ListConverter;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test List Converter that converts String from/to List<Long>
 */
public class LongListConverterTest {

    private static final String sampleString = "1,2,3,4,5,6,7,8,9,10,100,1000,10000,100000,1000000";
    private static final List<Long> sampleList = Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 100L, 1000L, 10000L, 100000L, 1000000L);

    private ListConverter mListConverter;

    @Before
    public void setup() {
        mListConverter = new ListConverter();
    }

    @Test
    public void convertStringToLongList() throws Exception {
        List<Long> convertedList = mListConverter.convertToEntityProperty(sampleString);
        for (int i = 0; i < sampleList.size(); i++) {
            Long convertedVal = convertedList.get(i);
            Long expectedVal = sampleList.get(i);
            assertThat(convertedVal).isEqualTo(expectedVal);
        }
    }

    @Test
    public void convertLongListToString() throws Exception {
        String convertedString = mListConverter.convertToDatabaseValue(sampleList);
        assertThat(convertedString).isEqualToIgnoringCase(sampleString);
    }

    @Test
    public void convertEmptyString() {
        List<Long> convertedList = mListConverter.convertToEntityProperty("");
        assertThat(convertedList).isNull();
    }

    @Test
    public void convertNullString() {
        List<Long> convertedList = mListConverter.convertToEntityProperty(null);
        assertThat(convertedList).isNull();
    }

    @Test
    public void convertEmptyList() {
        String convertedString = mListConverter.convertToDatabaseValue(new ArrayList<>());
        assertThat(convertedString).isNullOrEmpty();
    }

    @Test
    public void convertNullList() {
        String convertedString = mListConverter.convertToDatabaseValue(null);
        assertThat(convertedString).isNullOrEmpty();
    }
}