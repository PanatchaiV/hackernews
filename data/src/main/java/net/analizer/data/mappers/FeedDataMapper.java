package net.analizer.data.mappers;

import android.support.annotation.NonNull;

import net.analizer.data.greenDao.FeedItemEntity;
import net.analizer.domain.models.FeedItem;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public abstract class FeedDataMapper {

    public static FeedItem map(@NonNull FeedItemEntity entity) {

        return new FeedItem(
                entity.getId(),
                entity.getDeleted(),
                entity.getType(),
                entity.getBy(),
                entity.getTime(),
                entity.getText(),
                entity.getDead(),
                entity.getParent(),
                entity.getPoll(),
                entity.getKids(),
                entity.getUrl(),
                entity.getScore(),
                entity.getTitle(),
                entity.getParts(),
                entity.getDescendants()
        );
    }

    public static FeedItemEntity map(@NonNull FeedItem FeedItem) {

        return new FeedItemEntity(
                FeedItem.getItemId(),
                FeedItem.isDeleted(),
                FeedItem.getItemType(),
                FeedItem.getUserName(),
                FeedItem.getCreationTime(),
                FeedItem.getContent(),
                FeedItem.isDead(),
                FeedItem.getParentId(),
                FeedItem.getPollId(),
                FeedItem.getChildItemList(),
                FeedItem.getUrl(),
                FeedItem.getScore(),
                FeedItem.getTitle(),
                FeedItem.getRelatedPollList(),
                FeedItem.getCommentCount()
        );
    }

    public static List<FeedItem> mapToFeedItemList(@NonNull List<FeedItemEntity> feedItemList) {
        List<FeedItem> dataList = new ArrayList<>();
        for (FeedItemEntity entity : feedItemList) {
            dataList.add(map(entity));
        }

        return dataList;
    }

    public static List<FeedItemEntity> mapToFeedItemEntityList(@NonNull List<FeedItem> feedItemList) {
        List<FeedItemEntity> dataList = new ArrayList<>();
        for (FeedItem data : feedItemList) {
            dataList.add(map(data));
        }

        return dataList;
    }
}
