package net.analizer.data.models;

import android.support.annotation.NonNull;

import net.analizer.data.greenDao.DaoSession;
import net.analizer.domain.DataRepo;
import net.analizer.domain.DataStore;
import net.analizer.domain.models.FeedItem;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;

public class DataRepoImp implements DataRepo {

    private DaoSession mDaoSession;

    @Inject
    public DataRepoImp(DaoSession daoSession) {
        this.mDaoSession = daoSession;
    }

    @Override
    public Observable<FeedItem> getItemDetail(Long itemId) {
        DataStoreFactory factory = new DataStoreFactory(mDaoSession);
        DataStore dataStore = factory.createDataStore(new FeedItem(itemId));

        Observable<FeedItem> itemDetail = dataStore.getItemDetail(itemId);
        if (OnlineDataStoreImp.class.isAssignableFrom(dataStore.getClass())) {
            DataStore offlineDataStore = factory.createOfflineDataStore();
            return itemDetail.concatMap(offlineDataStore::save);
        }

        return itemDetail;
    }

    @Override
    public Observable<FeedItem> getTopStories() {
        DataStoreFactory factory = new DataStoreFactory(mDaoSession);
        DataStore dataStore = factory.createDataStore(null);
        Observable<FeedItem> topStories = dataStore.getTopStories();

        if (OnlineDataStoreImp.class.isAssignableFrom(dataStore.getClass())) {
            DataStore offlineDataStore = factory.createOfflineDataStore();
            return topStories.concatMap(offlineDataStore::save);
        }

        return topStories;
    }

    @Override
    public Observable<FeedItem> getComment(@NonNull FeedItem story) {

        DataStoreFactory factory = new DataStoreFactory(mDaoSession);
        FeedItem commentFeed = new FeedItem();
        commentFeed.parent = story.id;
        commentFeed.type = FeedItem.TYPE_COMMENT;

        DataStore dataStore = factory.createDataStore(commentFeed);
        Observable<FeedItem> comment = dataStore.getComment(story);

        if (dataStore instanceof OnlineDataStoreImp) {
            DataStore offlineDataStore = factory.createOfflineDataStore();
            return comment.concatMap(offlineDataStore::save);
        }

        return comment;
    }

    @Override
    public Observable<FeedItem> reloadStories() {
        DataStoreFactory factory = new DataStoreFactory(mDaoSession);
        DataStore onlineDataStore = factory.createOnlineDataStore();
        DataStore offlineDataStore = factory.createOfflineDataStore();

        return offlineDataStore
                .deleteAllStories()
                .toSingleDefault(new FeedItem())
                .toObservable()
                .concatWith(onlineDataStore.getTopStories());
    }

    @Override
    public Observable<FeedItem> reloadComments(@NonNull FeedItem story) {
        DataStoreFactory factory = new DataStoreFactory(mDaoSession);
        DataStore onlineDataStore = factory.createOnlineDataStore();
        DataStore offlineDataStore = factory.createOfflineDataStore();

        return offlineDataStore
                .getComment(story)
                .onErrorReturnItem(new FeedItem(-1L))
                .toList()
                .toObservable()
                .concatMap(feedItems -> offlineDataStore.deleteFeedItem(feedItems).toSingleDefault(new FeedItem()).toObservable())
                .concatMap(feedItem -> onlineDataStore.getComment(story));
    }

    @Override
    public Completable clearStories() {
        return Completable.fromAction(() -> {
            DataStoreFactory factory = new DataStoreFactory(mDaoSession);
            DataStore offlineDataStore = factory.createOfflineDataStore();
            offlineDataStore.deleteAllStories();
        });
    }

    @Override
    public Completable clearComments(@NonNull FeedItem story) {
        DataStoreFactory factory = new DataStoreFactory(mDaoSession);
        DataStore offlineDataStore = factory.createOfflineDataStore();

        Observable<FeedItem> feedItemObservable = offlineDataStore
                .getComment(story)
                .onErrorReturnItem(new FeedItem(-1L))
                .toList()
                .toObservable()
                .concatMap(feedItems -> offlineDataStore.deleteFeedItem(feedItems).toSingleDefault(new FeedItem()).toObservable());

        return Completable.fromObservable(feedItemObservable);
    }
}
