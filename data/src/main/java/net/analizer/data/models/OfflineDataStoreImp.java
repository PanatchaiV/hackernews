package net.analizer.data.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.analizer.data.greenDao.DaoSession;
import net.analizer.data.greenDao.FeedItemEntity;
import net.analizer.data.greenDao.FeedItemEntityDao;
import net.analizer.data.mappers.FeedDataMapper;
import net.analizer.domain.DataStore;
import net.analizer.domain.models.FeedItem;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;

public class OfflineDataStoreImp implements DataStore {

    private DaoSession mDaoSession = null;

    @Inject
    public OfflineDataStoreImp(DaoSession daoSession) {
        this.mDaoSession = daoSession;
    }

    @Override
    public Observable<FeedItem> getTopStories() {

        return Observable
                .fromCallable(() -> {
                    QueryBuilder<FeedItemEntity> builder = mDaoSession.queryBuilder(FeedItemEntity.class);
                    List<FeedItemEntity> entityList = builder.where(FeedItemEntityDao.Properties.Type.notEq(FeedItem.TYPE_COMMENT))
                            .orderAsc(FeedItemEntityDao.Properties.Time)
                            .list();

                    if (entityList != null) {
                        return entityList;
                    }

                    throw new Exception("Database is empty");
                })
                .flatMapIterable(list -> list)
                .flatMap(feedItemEntity -> Observable.just(FeedDataMapper.map(feedItemEntity)));
    }

    @Override
    public Observable<FeedItem> getItemDetail(@NonNull Long itemId) {
        return Observable.fromCallable(() -> {

            QueryBuilder<FeedItemEntity> builder = mDaoSession.queryBuilder(FeedItemEntity.class);
            FeedItemEntity feedItemEntity = builder.where(FeedItemEntityDao.Properties.Id.eq(itemId)).unique();

            if (feedItemEntity != null) {
                return FeedDataMapper.map(feedItemEntity);
            }

            throw new Exception(String.format("%s not found", itemId));
        });
    }

    @Override
    public Observable<FeedItem> getComment(@NonNull FeedItem story) {
        return Observable
                .fromCallable(() -> {

                    QueryBuilder<FeedItemEntity> builder = mDaoSession.queryBuilder(FeedItemEntity.class);
                    List<FeedItemEntity> entityList = builder
                            .where(FeedItemEntityDao.Properties.Type.eq(FeedItem.TYPE_COMMENT))
                            .where(FeedItemEntityDao.Properties.Parent.eq(story.id))
                            .orderAsc(FeedItemEntityDao.Properties.Time)
                            .list();

                    if (entityList != null) {
                        return entityList;
                    }

                    throw new Exception(String.format("There are no comments for the topic '%s'", story.title));
                })
                .flatMapIterable(list -> list)
                .flatMap(feedItemEntity -> Observable.just(FeedDataMapper.map(feedItemEntity)));
    }

    @Override
    public Observable<FeedItem> save(@NonNull FeedItem feedItem) {
        mDaoSession.getFeedItemEntityDao().save(FeedDataMapper.map(feedItem));
        return Observable.fromCallable(() -> {
            long l = mDaoSession.insertOrReplace(FeedDataMapper.map(feedItem));
            if (l >= 0) {
                return feedItem;
            }

            throw new Exception("Cannot insertOrReplace " + String.valueOf(feedItem.id));
        });
    }

    @Override
    public Completable deleteAllStories() {
        return Completable.fromAction(() -> mDaoSession.deleteAll(FeedItemEntity.class));
    }

    @Override
    public Completable deleteFeedItem(@Nullable List<FeedItem> FeedItemList) {
        return Completable.fromAction(() -> {
                    FeedItemEntityDao feedItemEntityDao = mDaoSession.getFeedItemEntityDao();
                    List<FeedItemEntity> feedItemEntities = FeedDataMapper.mapToFeedItemEntityList(FeedItemList);
                    feedItemEntityDao.deleteInTx(feedItemEntities);
                }
        );
    }
}
