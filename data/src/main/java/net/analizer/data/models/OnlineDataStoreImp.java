package net.analizer.data.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.analizer.domain.AppConfig;
import net.analizer.domain.DataStore;
import net.analizer.domain.models.FeedItem;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OnlineDataStoreImp implements DataStore {

    private RetrofitInterface mRetrofitInterface;

    @Inject
    public OnlineDataStoreImp() {
        Gson gson = new GsonBuilder().create();
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.BASE_DOMAIN)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        mRetrofitInterface = retrofit.create(RetrofitInterface.class);
    }

    @Override
    public Observable<FeedItem> getTopStories() {

        return mRetrofitInterface.getTopStories()
                .flatMapIterable(storyIdList -> storyIdList)
                .flatMap(storyId -> mRetrofitInterface.getFeedItem(storyId));
    }

    @Override
    public Observable<FeedItem> getItemDetail(@NonNull Long itemId) {
        return mRetrofitInterface.getFeedItem(itemId);
    }

    @Override
    public Observable<FeedItem> getComment(@NonNull FeedItem story) {
        List<Long> commentList = story.getChildItemList();
        if (commentList != null && commentList.size() > 0) {
            Observable<Long> longObservable = Observable.fromIterable(commentList);
            return longObservable.flatMap(itemId -> mRetrofitInterface.getFeedItem(itemId));
        }

        return Observable.error(new Throwable("No Comment"));
    }

    @Override
    public Observable<FeedItem> save(@NonNull FeedItem FeedItem) {
        //do nothing
        return Completable.complete().toObservable();
    }

    @Override
    public Completable deleteAllStories() {
        //do nothing
        return Completable.complete();
    }

    @Override
    public Completable deleteFeedItem(@Nullable List<FeedItem> FeedItemList) {
        //do nothing
        return Completable.complete();
    }
}
