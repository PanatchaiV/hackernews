package net.analizer.data.models;

import android.support.annotation.Nullable;

import net.analizer.data.greenDao.DaoSession;
import net.analizer.data.greenDao.FeedItemEntity;
import net.analizer.data.greenDao.FeedItemEntityDao;
import net.analizer.domain.DataStore;
import net.analizer.domain.models.FeedItem;

import org.greenrobot.greendao.query.QueryBuilder;

import javax.inject.Inject;

@SuppressWarnings("WeakerAccess")
public class DataStoreFactory {

    private DaoSession mDaoSession = null;

    @Inject
    public DataStoreFactory(DaoSession daoSession) {
        this.mDaoSession = daoSession;
    }

    public DataStore createDataStore(@Nullable FeedItem feedItem) {
        if (feedItem != null) {

            if (feedItem.type == null || feedItem.type.length() == 0) {
                return createDataStoreById(feedItem.id);

            } else if (feedItem.type.equalsIgnoreCase(FeedItem.TYPE_COMMENT)) {
                return createCommentDataStore(feedItem.parent);
            }

            return createStoryDataStore(feedItem.id);
        }

        return createStoryDataStore(null);
    }

    private DataStore createDataStoreById(Long itemId) {
        if (itemId != null) {
            QueryBuilder<FeedItemEntity> builder = mDaoSession.queryBuilder(FeedItemEntity.class);
            QueryBuilder<FeedItemEntity> where = builder.where(FeedItemEntityDao.Properties.Id.eq(itemId));

            long count = where.count();
            if (count > 0) {
                return new OfflineDataStoreImp(mDaoSession);
            }
        }

        return new OnlineDataStoreImp();
    }

    private DataStore createStoryDataStore(Long storyId) {
        QueryBuilder<FeedItemEntity> builder = mDaoSession.queryBuilder(FeedItemEntity.class);
        QueryBuilder<FeedItemEntity> where = builder.where(FeedItemEntityDao.Properties.Type.notEq(FeedItem.TYPE_COMMENT));
        if (storyId != null) {
            where = where.where(FeedItemEntityDao.Properties.Id.eq(storyId));
        }

        long count = where.count();

        if (count > 0) {
            return new OfflineDataStoreImp(mDaoSession);
        }

        return new OnlineDataStoreImp();
    }

    private DataStore createCommentDataStore(Long storyId) {

        if (storyId != null) {
            QueryBuilder<FeedItemEntity> builder = mDaoSession.queryBuilder(FeedItemEntity.class);
            long count = builder
                    .where(FeedItemEntityDao.Properties.Type.eq(FeedItem.TYPE_COMMENT))
                    .where(FeedItemEntityDao.Properties.Parent.eq(storyId))
                    .count();

            if (count > 0) {
                return new OfflineDataStoreImp(mDaoSession);
            }
        }

        return new OnlineDataStoreImp();
    }

    public DataStore createOnlineDataStore() {
        return new OnlineDataStoreImp();
    }

    public DataStore createOfflineDataStore() {
        return new OfflineDataStoreImp(mDaoSession);
    }
}
