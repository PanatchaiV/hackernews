package net.analizer.data.models;

import net.analizer.domain.models.FeedItem;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RetrofitInterface {

    @GET("topstories.json")
    Observable<List<Long>> getTopStories();

    @GET("item/{itemId}.json")
    Observable<FeedItem> getFeedItem(@Path("itemId") Long itemId);
}