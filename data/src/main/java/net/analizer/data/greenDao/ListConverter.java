package net.analizer.data.greenDao;

import org.greenrobot.greendao.converter.PropertyConverter;

import java.util.ArrayList;
import java.util.List;

public class ListConverter implements PropertyConverter<List<Long>, String> {

    @Override
    public List<Long> convertToEntityProperty(String databaseValue) {
        if (databaseValue == null || databaseValue.length() == 0) {
            return null;

        } else {
            List<Long> valList = new ArrayList<>();
            String[] split = databaseValue.split(",");
            for (String val : split) {
                valList.add(Long.parseLong(val));
            }
            return valList;
        }
    }

    @Override
    public String convertToDatabaseValue(List<Long> entityProperty) {
        if (entityProperty == null || entityProperty.size() == 0) {
            return null;

        } else {

            StringBuilder sb = new StringBuilder();
            for (Long val : entityProperty) {
                sb.append(val.toString());
                sb.append(",");
            }
            return sb.substring(0, sb.length() - 1);
        }
    }
}
