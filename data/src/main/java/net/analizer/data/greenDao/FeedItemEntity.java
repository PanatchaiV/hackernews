package net.analizer.data.greenDao;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.List;

@Entity(nameInDb = "FeedItem")
public class FeedItemEntity {

    @Id
    private Long id;//  The item's unique id.

    @Property(nameInDb = "deleted")
    private Boolean deleted;//  true if the item is deleted.

    @Property(nameInDb = "type")
    private String type;//  The type of item. One of "job", "story", "comment", "poll", or "pollopt".

    @Property(nameInDb = "by")
    private String by;//  The username of the item's author.

    @Property(nameInDb = "time")
    private Long time;//  Creation date of the item, in Unix Time.

    @Property(nameInDb = "text")
    private String text;//  The comment, story or poll text. HTML.

    @Property(nameInDb = "dead")
    private Boolean dead;//  true if the item is dead.

    @Property(nameInDb = "parent")
    private Long parent;//  The comment's parent: either another comment or the relevant story.

    @Property(nameInDb = "poll")
    private Long poll;//  The pollopt's associated poll.

    @Convert(converter = ListConverter.class, columnType = String.class)
    @Property(nameInDb = "kids")
    private List<Long> kids;//  The ids of the item's comments, in ranked display order.

    @Property(nameInDb = "url")
    private String url;//  The URL of the story.

    @Property(nameInDb = "score")
    private Integer score;//  The story's score, or the votes for a pollopt.

    @Property(nameInDb = "title")
    private String title;//  The title of the story, poll or job.

    @Convert(converter = ListConverter.class, columnType = String.class)
    @Property(nameInDb = "parts")
    private List<Long> parts;//  A list of related pollopts, in display order.

    @Property(nameInDb = "descendants")
    private Long descendants;//  In the case of stories or polls, the total comment count.

    @Generated(hash = 635610220)
    public FeedItemEntity(Long id, Boolean deleted, String type, String by, Long time, String text,
                          Boolean dead, Long parent, Long poll, List<Long> kids, String url, Integer score,
                          String title, List<Long> parts, Long descendants) {
        this.id = id;
        this.deleted = deleted;
        this.type = type;
        this.by = by;
        this.time = time;
        this.text = text;
        this.dead = dead;
        this.parent = parent;
        this.poll = poll;
        this.kids = kids;
        this.url = url;
        this.score = score;
        this.title = title;
        this.parts = parts;
        this.descendants = descendants;
    }

    @Generated(hash = 888315941)
    public FeedItemEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBy() {
        return this.by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public Long getTime() {
        return this.time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getDead() {
        return this.dead;
    }

    public void setDead(Boolean dead) {
        this.dead = dead;
    }

    public Long getParent() {
        return this.parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Long getPoll() {
        return this.poll;
    }

    public void setPoll(Long poll) {
        this.poll = poll;
    }

    public List<Long> getKids() {
        return this.kids;
    }

    public void setKids(List<Long> kids) {
        this.kids = kids;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getScore() {
        return this.score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Long> getParts() {
        return this.parts;
    }

    public void setParts(List<Long> parts) {
        this.parts = parts;
    }

    public Long getDescendants() {
        return this.descendants;
    }

    public void setDescendants(Long descendants) {
        this.descendants = descendants;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FeedItemEntity) {
            FeedItemEntity feedItem = (FeedItemEntity) obj;
            return feedItem.id != null && feedItem.id.equals(id);
        }

        return false;
    }

    @Override
    public String toString() {
        return "FeedItemEntity{" +
                "id=" + id +
                ", deleted=" + deleted +
                ", type='" + type + '\'' +
                ", by='" + by + '\'' +
                ", time=" + time +
                ", text='" + text + '\'' +
                ", dead=" + dead +
                ", parent=" + parent +
                ", poll=" + poll +
                ", kids=" + kids +
                ", url='" + url + '\'' +
                ", score=" + score +
                ", title='" + title + '\'' +
                ", parts=" + parts +
                ", descendants=" + descendants +
                '}';
    }
}
